export const ERROR = 'ERROR'
export const SET_ERROR = `${ERROR} SET`
export const REMOVE_ERROR = `${ERROR} REMOVE`

export const setError = (msg) => ({
    type: SET_ERROR,
    payload: msg
})

export const removeError = () => ({
    type: REMOVE_ERROR,
})