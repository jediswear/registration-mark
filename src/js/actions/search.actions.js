export const CARS_DATA = 'CARS_DATA'
export const CARS_DATA_FETCH = `${CARS_DATA} FETCH`
export const CARS_DATA_SET = `${CARS_DATA} SET`

export const fetchData = (query) => ({
    type: CARS_DATA_FETCH,
    payload: query
})

export const dataSet = (data) => ({
    type: CARS_DATA_SET,
    payload: data
})