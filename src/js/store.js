import {createStore, applyMiddleware} from 'redux'
import searchReducer from './reducers/search.reducer'
import appMiddleware from './middleware/app'
import coreMiddleware from './middleware/core'

const middleware = [
    ...appMiddleware,
    ...coreMiddleware
]

const store = createStore(searchReducer, applyMiddleware(...middleware))

export default store