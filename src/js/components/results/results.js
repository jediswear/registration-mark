import React from 'react'
import {connect} from 'react-redux'

const Results = ({carsData, error}) => {

    const carKeys = Object.keys(carsData)

    const translations = {
        owner: 'Владелец',
        year: 'Год производства',
        ownersCount: 'Колличетсво владельцев',
        crashesCount: 'Факты ДДП',
    }

    const content = carKeys.map((carKey, i) => {
        return (
            <tr className='results-table__row'
                key={i}>
                <td className='results-table__col'>{translations[carKey]}:</td>
                <td className='results-table__col'>{carsData[carKey]}</td>
            </tr>
        )
    })

    return (
        <div className='results'>
            {
                !error && carsData
                    ?
                    <table className='results-table'>
                        <tbody>
                        {content}
                        </tbody>
                    </table>
                    :
                    <span className="results__message">
                    Введите номерной знак автомобиля (например AA9999AI) и нажмите Enter
                </span>

            }
        </div>
    )
}

const mapStateToProps = ({carsData, carsError}) => ({
    carsData,
    error: carsError
})

export default connect(mapStateToProps)(Results)