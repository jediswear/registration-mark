import React from 'react'
import {connect} from 'react-redux'
import {fetchData} from '../../actions/search.actions'
import {setError} from '../../actions/error.actions'

const Search = ({carsError, search, setError}) => {

    const changeHandler = (e) => {

        const value           = e.target.value,
              carNumberRegExp = /^[A-ZА-Я]{2}[0-9]{4}[A-ZА-Я]{2}$/,
              valid           = carNumberRegExp.test(value)

        if (e.key === 'Enter') {
            valid && search(value)
            !valid && setError('Некоректный формат номера')
        }
    }

    return (
        <div className='search'>
            <span className="search__title">Проверка авто по номерному знаку</span>
            <input
                type="text"
                placeholder='Номерной знак авто'
                className="search__input"
                onKeyDown={e => changeHandler(e)}
            />
            <span className='search__error'>
                    {carsError && `Ошибка: ${carsError}`}
                </span>
        </div>
    )
}

const mapStateToProps = ({carsError}) => ({
    carsError
})

const mapDispatchToProps = {
    search: fetchData,
    setError
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)