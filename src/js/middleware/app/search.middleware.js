import {API_ERROR, API_SUCCESS, apiRequest} from '../../actions/api.actions'
import {CARS_DATA, CARS_DATA_FETCH, dataSet} from '../../actions/search.actions'
import {removeError, setError} from '../../actions/error.actions'

export default ({dispatch}) => next => action => {
    next(action)

    const {type, payload} = action

    switch (type) {
        case CARS_DATA_FETCH:
            dispatch(apiRequest(
                CARS_DATA,
                `/api/v1/car-info/${payload}`,
                'GET',
            ))
            break
        case `${CARS_DATA} ${API_SUCCESS}`:
            dispatch(removeError())
            dispatch(dataSet(payload))
            break
        case `${CARS_DATA} ${API_ERROR}`:
            dispatch(setError(payload))
            break
    }
}