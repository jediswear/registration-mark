import {apiSuccess, apiError} from '../../actions/api.actions'

export default ({getState, dispatch}) => next => action => {
    next(action)

    if (action.type.includes('API_REQUEST')) {

        const {meta} = action

        let error = false

        fetch(meta.url)
            .then(res => {

                if (!res.ok)
                    error = true

                return res.json()
            })
            .then(data => {

                if (error){
                    dispatch(apiError(meta.type, data))
                    throw new Error('An error was happened')
                }

                dispatch(apiSuccess(meta.type, data))
            })
            .catch(err => err)
    }
}