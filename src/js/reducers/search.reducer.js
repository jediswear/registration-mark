import {CARS_DATA_SET} from '../actions/search.actions'
import {REMOVE_ERROR, SET_ERROR} from '../actions/error.actions'

const initialState = {
    carsData: ''
}

const searchReducer = (state = initialState, action) => {

    const {payload, type} = action

    switch (type) {
        case CARS_DATA_SET:
            return {
                ...state,
                carsData: payload.result
            }
            break
        case SET_ERROR:
            return {
                ...state,
                carsError: payload
            }
            break
        case REMOVE_ERROR:
            return {
                ...state,
                carsError: null
            }
            break
        default:
            return state
    }
}

export default searchReducer