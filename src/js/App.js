import React, {Component} from 'react'
import Search from './components/search'
import Results from './components/results'

export default class App extends Component {
    render() {
        return (
            <>
                <section className="header">
                    <Search/>
                </section>
                <section className="content">
                    <Results/>
                </section>
            </>
        )
    }
}